# Changelog
## [Unreleased]
### Fixed
- Improved performance of /api/vod/related 4x. Now related vods are filtered based on similar genres only. 

## [1.5.6] - 15/12/2020
### Fixed
- Fix package deletion issue at channels
- Fix genre deletion issue at tv series
- Refactored channel event api to use cache
- Removed subscription filter from feed api when the customer has no subscription
- Fix winston to use with transporters
- Fix settings problem with expiration date
- Fix apiv4 verify user middleware with promises
- Fix no company condition in public subscription apis
- etc...

### Added
- Add apiv4 endpoints
- Add apiv4 endpoint for auto login
- Add docker to magoware-backoffice
- Add web-app app id.
- Added app id 8
- Unified epg store for home page and channel event
- Added public api for getting last customer subscription
- Added api: /apiv2/channels/scheduled
- Use new config system in api: /apiv4/proxy/tibo
- Use new config system in api: /apiv2/settings/settings
- Use new config system in api: /apiv2/sites/registration
- Use new config system in api: /apiv2/payments/stripe/charge
- Use new config system in api: /file-upload/single-file/:model/:field, /file-upload/multi-file
- Use new config system in epg import
- Use new config system in api: /apiv2/drm/ezdrm/license
- Use new config system in stream token apis:
  - /apiv2/token/akamaitokenv2hdnts/*
  - /apiv2/token/akamaitokenv2/*
  - /apiv2/token/akamaitokenv2extraquery/*
  - /apiv2/token/catchupakamaitokenv2/*
  - /apiv2/token/flussonic/*
  - /apiv2/token/nimble/*
  - /apiv2/token/verizon/*
  - /apiv4/token/verizon/*
  - /apiv2/token/generatewowzatoken/*
  - /apiv2/drm/nimble
  - /apiv4/token/akamaisegmentmedia/*
  - /apiv4/token/akamaitokenv2nimble/*


## [1.0.0] - 25/09/2020
### Added
- Changelog.
- Semver version commands.

[Unreleased]: https://bitbucket.org/magoware/magoware-backoffice/branch/develop
[1.0.0]: https://bitbucket.org/magoware/magoware-backoffice/src/v1.0.0/
[1.5.6]: https://bitbucket.org/magoware/magoware-backoffice/src/v1.5.5/