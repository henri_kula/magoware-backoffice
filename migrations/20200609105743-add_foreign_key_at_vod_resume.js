'use strict';

var winston = require('winston'),
    path = require('path'),
    utils = require(path.resolve('./custom_functions/migration_utils'));


module.exports = {
    up: (queryInterface, Sequelize) => {
        return utils.getColumnFK(queryInterface, 'vod_resume', 'login_id')
            .then(function(fks) {
                if(fks.length == 0) {
                    return addFK();
                }
            })
        function addFK() {
            return queryInterface.sequelize.query("alter table vod_resume add foreign key (login_id) references login_data(id)")
        }
    },

    down: (queryInterface, Sequelize) => {
        return Promise.resolve();
    }
}
