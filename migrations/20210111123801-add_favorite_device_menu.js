'use strict';

const _ = require('lodash');
const deviceMenu = require('../config/defaultvalues/vod_menu.json')

module.exports = {
  up: async (queryInterface, Sequelize) => {
    try {
      let compMissingMenu = await queryInterface.sequelize.query('SELECT id FROM settings WHERE id != -1 AND id NOT IN (SELECT company_id FROM vod_menu WHERE `vod_menu`.`name` = "Favorites");')
      compMissingMenu = compMissingMenu[0];
      const menu = findMenuByName(deviceMenu, 'Favorites');
      if (!menu) {
        throw new Error('Menu not found');
      }

      let menus = [];
      compMissingMenu.forEach(comp => {
        menus.push(cloneMenu(menu, comp.id))
      });

      if (menus.length == 0) {
        return;
      }
      await queryInterface.bulkInsert('vod_menu', menus);
    }
    catch (err) {
      throw err;
      console.log(err);
    }
  },

  down: async (queryInterface, Sequelize) => {
    try {
      await queryInterface.sequelize.query('DELETE FROM vod_menu WHERE name="Favorites";')
    }
    catch (err) {
      console.log(err);
    }
  }
};



function findMenuByName(menus, name) {
  for (let menu of menus) {
    if (menu.name == name) {
      return menu;
    }
  }

  return undefined;
}

function cloneMenu(menu, companyId) {
  let defMenu = _.cloneDeep(menu);
  let date = new Date();
  defMenu.company_id = companyId;
  defMenu.createdAt = date;
  defMenu.updatedAt = date

  return defMenu;
}