'use strict';

var winston = require('winston'),
    config = require('../config/env/db.connection');

module.exports = {
  up: (queryInterface, Sequelize) => {
    let affectedTables = [
      'commands',
      'devices',
      'favorite_channels',
      'program_schedule',
      'salesreport',
      't_tv_series_sales',
      't_vod_sales',
      'tv_episode_resume',
      'vod_resume'
    ];

    let tablesWithSetNull = [
      'salesreport',
      't_tv_series_sales',
      't_vod_sales',
      'tv_episode_resume',
      'vod_resume'
    ];

    return getForeignKeyMetadata(config.database, 'login_data', 'id')
      .then(function(metas) {
        return dropForeignKeyConstraints(metas, affectedTables)
          .then(function() {
            return makeLoginFkColumnNullable(metas, tablesWithSetNull)
              .then(function() {
                let promises = [];
                for (let tableName of affectedTables) {
                  let metadata = metas[tableName];
                  if (metadata) {
                    let onDelete = 'CASCADE';
                    let onUpdate = 'CASCADE';

                    if (tablesWithSetNull.indexOf(tableName) != -1) {
                      onDelete = 'SET NULL';
                    }

                    let sql = 'ALTER TABLE ' + tableName +' ADD CONSTRAINT ' + metadata.constraint_name + ' FOREIGN KEY (`' + metadata.column_name + '`) REFERENCES login_data(id) ON DELETE ' + onDelete + ' ON UPDATE ' + onUpdate;
                    console.log(sql)
                    promises.push(queryInterface.sequelize.query(sql));
                  }
                }

                return Promise.all(promises);
              })
          });
      }).catch(function(err) {
        console.log(err);
        throw err;
      })

    function dropForeignKeyConstraints(metas, tableNames) {
      let promises = [];

      for (let tableName of tableNames) {
        let metadata = metas[tableName];
        if (metadata) {
          let sql = 'ALTER TABLE ' + tableName + ' DROP FOREIGN KEY ' + metadata.constraint_name;
          console.log(sql)
          promises.push(queryInterface.sequelize.query(sql));
        }
      }

      return Promise.all(promises);
    }

    function makeLoginFkColumnNullable(metas, tableNames) {
      let promises = [];
      for (let tableName of tableNames) {
        let metadata = metas[tableName];
        if (metadata) {
          promises.push(queryInterface.changeColumn(tableName, metadata.column_name, {
            type: Sequelize.INTEGER(11),
            allowNull: true
          }));
        }
      }

      return Promise.all(promises);
    }

    function getForeignKeyMetadata(database, referencedTable, referencedColumn) {
      let metadataQuery = `
        SELECT 
          TABLE_NAME,COLUMN_NAME,CONSTRAINT_NAME, REFERENCED_TABLE_NAME,REFERENCED_COLUMN_NAME
        FROM
          INFORMATION_SCHEMA.KEY_COLUMN_USAGE
        WHERE
          REFERENCED_TABLE_SCHEMA = '` + database + `' AND
          REFERENCED_TABLE_NAME = '` + referencedTable + `' AND
          REFERENCED_COLUMN_NAME = '` + referencedColumn + `';`;
      return queryInterface.sequelize.query(metadataQuery)
        .then(function(rows) {
          if (rows.length > 0) {
            let metadata = {};

            for (const row of rows[0]) {
              let m = {
                column_name: row.COLUMN_NAME,
                constraint_name: row.CONSTRAINT_NAME,
              }

              metadata[row.TABLE_NAME] = m;
            }

            return metadata;
          }

          return null;
        });
    }
  },

  down: (queryInterface, Sequelize) => { 
    return Promise.resolve();
  }
};
