'use strict';

const _ = require('lodash');
const defaultConfigs = require('../config/defaultvalues/configs.json');


module.exports = {
  up: async (queryInterface, Sequelize) => {
    try {
      let count = await queryInterface.sequelize.query('SELECT COUNT(id) as counter FROM configs')
      if (count[0][0].counter > 0) {
        return;
      }

      let companies = await queryInterface.sequelize.query('SELECT id FROM settings WHERE id > -2');
      let configs = [];
      companies[0].forEach((company) => {
        let companyConfigs = createDefaultConfigs(company.id);
        configs.push(...companyConfigs);
      });

      await queryInterface.bulkInsert('configs', configs, {}, {value: {type: new Sequelize.JSON()}});
    }
    catch(err) {
      console.log(err)
    }
  },

  down: async (queryInterface, Sequelize) => {
    
  }
};

function createDefaultConfigs(companyId) {
  let defConfigs = _.cloneDeep(defaultConfigs);
  let date = new Date();
  for (let i = 0; i < defConfigs.length; i++) {
    defConfigs[i].company_id = companyId;
    defConfigs[i].createdAt = date;
    defConfigs[i].updatedAt = date
  }

  return defConfigs;
}
