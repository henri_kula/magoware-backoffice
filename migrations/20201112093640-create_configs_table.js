'use strict';

const winston = require('winston');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    try {
      await queryInterface.createTable('configs', {
        id: {
          type: Sequelize.INTEGER(11),
          allowNull: false,
          primaryKey: true,
          autoIncrement: true,
          unique: true
        },
        company_id: {
          type: Sequelize.INTEGER(11),
          allowNull: false,
          references: {
            model: 'settings',
            key: 'id'
          }
        },
        key: {
          type: Sequelize.STRING(20),
          allowNull: false,
        },
        category: {
          type: Sequelize.STRING(20),
          allowNull: false,
        },
        description: {
          type: Sequelize.STRING(255),
          allowNull: false
        },
        value: {
          type: Sequelize.JSON,
          allowNull: false,
        },
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE,
      }, {
        uniqueKeys: {
          unique_company_id_and_key: {
            fields: ['company_id', 'key']
          }
        }
      });
    }
    catch (err) {
      winston.error('Creating table failed with error ', err);
    }
  },

  down: async (queryInterface, Sequelize) => {
    try {
      queryInterface.dropTable('configs');
    }
    catch (err) {
      winston.error("Failed to drop table with error ", err);
    }
  }
};
