import temp from './index.html';

function bullBoard($stateProvider) {

    $stateProvider.state('bullboard', {
        parent: 'main',
        url: '/bullboard',
        controller: ['$http', '$scope', 'notification', function ($http, $scope, notification) {

            $scope.link = window.location.protocol + '//' + window.location.hostname + ':' + window.location.port + '/api/bull/board';

        }],
        template: temp
    });
}

bullBoard.$inject = ['$stateProvider'];

export default bullBoard;