import edit_button from '../edit_button.html';
import filter_genre_btn from '../filter_genre_btn.html';
import modalImageUpload from "../../templates/modalImageUpload.html";

export default function (nga, admin) {
    let mediaplayer = admin.getEntity('mediaplayer');
    mediaplayer.listView()
        .title('<h4>Media Player <i class="fa fa-angle-right" aria-hidden="true"></i> Edit</h4>')
        .fields([
            nga.field('player_name')
                .label('Player Name'),
            nga.field('app_id')
                .label('APP ID'),
            nga.field('default','boolean')
                .label('Default'),
        ])
        .listActions(['edit'])


    mediaplayer.creationView()
        .title('<h4>Media Player <i class="fa fa-angle-right" aria-hidden="true"></i> Create</h4>')
        .onSubmitSuccess(['progression', 'notification', '$state', 'entry', 'entity', function (progression, notification, $state, entry, entity) {
            notification.log(`Ads send successfully`, {addnCls: 'humane-flatty-success'});
            // redirect to the list view
            $state.go($state.current, {}, {reload: true})
                .then($state.go($state.get('list'), {entity: entity.name()})); // cancel the default action (redirect to the edition view)
            return false;
        }])
        .fields([
            nga.field('app_id', 'choice')
                .attributes({ placeholder: 'Choose from dropdown list App ID' })
                .validation({
                    validator: function(value) {
                        if (value === null || value === '') {
                            throw new Error('Please Select App ID')
                        }
                    },
                    required: true
                })
                .choices([
                    { value: 1, label: 'Android Set Top Box' },
                    { value: 2, label: 'Android Mobile' },
                    { value: 3, label: 'iOS Mobile' },
                    { value: 4, label: 'Android Smart TV' },
                    { value: 5, label: 'Web TV' },
                    { value: 6, label: 'Apple TV' },
                    { value: 7, label: 'Web Smart Tv' },
                    { value: 8, label: 'Web App' },

                ])
                .validation({ required: true })
                .label('App ID'),
            nga.field('default','boolean')
                .attributes({ placeholder: 'Default Player' })
                .validation({ required: true })
                .label('Default Player'),
            nga.field('player_name')
                .attributes({ placeholder: 'Player Name' })
                .defaultValue('default')
                .validation({ required: true })
                .label('Player Name'),
            nga.field('template')
                .label('')
                .template(edit_button),
        ]);

    mediaplayer.editionView()
        .title('<h4>Media Player <i class="fa fa-angle-right" aria-hidden="true"></i> Edit: {{ entry.values.username }}</h4>')
        .actions(['list'])
        .fields([
            mediaplayer.creationView().fields(),
        ]);

    return mediaplayer;

}