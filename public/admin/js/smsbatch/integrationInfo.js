function integrationInfo(Restangular, $uibModal, $q, notification, $state,$http) {
    'use strict';

    return {
        restrict: 'E',
        scope: {
            selection: '=',
            type: '@',
            dismiss: '&'
        },
        link: function(scope, element, attrs) {

            scope.icon = 'fa fa-info';
            //scope.icon = 'fa fa-eye';

            if (attrs.type === 'integration-info') scope.label = 'Info';
                scope.modal = function () {
                    if(attrs.id === 'vod'){
                        $uibModal.open({
                            template:
                                '<div class="modal-header">' +
                                '<h5 class="modal-title" style="font-weight: bold;font-size: 20px;">More Information</h5>' +
                                '</div>' +
                                '<div class="modal-body"> <p> Video On Demand is media distribution that allows to access videos whenever it suits for you.' +
                                '<br> Allows to select and watch video or audio content on demand.<br><br>' +
                                'Limit Carousel Movies Field is the number of movies into a carousel displayed in VOD.\n' +
                                'If the number is more than the one inserted below than in device it displays the "Show More" button. </p>'+

                                '<div class="row">'+
                                '<form>'+
                                '<div class="form-group" style="padding: 20px;">'+
                                '</div>'+
                                '</form>'+
                                '</div>'+
                                '</div>'+
                                '<div class="modal-footer">' +
                                '<button class="btn btn-primary" type="button" ng-click="ok()">OK</button>' +
                                '</div>',

                            controller:('main', ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
                                function closeModal() {
                                    $uibModalInstance.dismiss();
                                }

                                $scope.ok = function () {
                                    closeModal();
                                }
                            }])
                        })
                    }
                    if (attrs.id === 'woocomerce'){
                        $uibModal.open({
                            template:
                                '<div class="modal-header">' +
                                '<h5 class="modal-title" style="font-weight: bold;font-size: 20px;">More Information</h5>' +
                                '</div>' +
                                '<div class="modal-body"> <p>Integration Configuration of WooCommerce Billing System. It is a system for clients who wouldn\'t be able to make the payment right after purchase that\'s why it is called "Pay By invoice".' +
                                ' In other standarts payment methods, payment is not an option. With WooCommerce you can enable the payment method so the customers will be able to make payment within the interval allowed in WooCommerce Invoice System. Invoice payment takes place only when customers receive the invoice including details regarding the amount due and the date of payment of the amount.\n' +
                                '<br><br>WooCommerce Base URL\n' +
                                '<br>Base URL of the WooCommerce Page (http(s)://yourdomainname).\n' +
                                '<br><br>WooCommerce Consumer Key\n' +
                                '<br>Consumer Key is used to identify your account with WooCommerce. Key is found in WooCommerce Admin Page at Settings Menu.\n' +
                                'At Keys/Apps and than Webhooks you can generate the API Keys.\n' +
                                '<br><br>WooCommerce Consumer Secret\n' +
                                '<br>Consumer Secret Key is important to get the authentication. Key is found in WooCommerce Admin Page at Settings Menu.\n' +
                                'At Keys/Apps and than Webhooks you can generate the API Keys.</p>'+

                                '<div class="row">'+
                                '<form>'+
                                '<div class="form-group" style="padding: 20px;">'+
                                '</div>'+
                                '</form>'+
                                '</div>'+
                                '</div>'+
                                '<div class="modal-footer">' +
                                '<button class="btn btn-primary" type="button" ng-click="ok()">OK</button>' +
                                '</div>',

                            controller:('main', ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
                                function closeModal() {
                                    $uibModalInstance.dismiss();
                                }

                                $scope.ok = function () {
                                    closeModal();
                                }
                            }])
                        })
                    }
                    if(attrs.id === 'stripe'){
                        $uibModal.open({
                            template:
                                '<div class="modal-header">' +
                                '<h5 class="modal-title" style="font-weight: bold;font-size: 20px;">More Information</h5>' +
                                '</div>' +
                                '<div class="modal-body"> <p> Integration Configuration of Stripe Billing System which is the fastest way to pay for Subscriptions or VOD Movie.\n' +
                                'API keys are important to authenticate requests with Stripe.\n' +
                                '<br><br> Stripe API Key is used for the integration to operate securely.' +
                                '<br><br>Publishable API Key is important to identify your account with Stripe. It is not a secret key so it is safely to be published in your App.\n' +
                                'Publishable Key is available in Stripe Admin Page under Development tools.\n' +
                                '<br><br>Stripe Secret Key should be kept confidential and stored on your own servers.\n' +
                                'Secret Key is found in Stripe Admin Page under Development tools. </p>'+

                                '<div class="row">'+
                                '<form>'+
                                '<div class="form-group" style="padding: 20px;">'+
                                '</div>'+
                                '</form>'+
                                '</div>'+
                                '</div>'+
                                '<div class="modal-footer">' +
                                '<button class="btn btn-primary" type="button" ng-click="ok()">OK</button>' +
                                '</div>',

                            controller:('main', ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
                                function closeModal() {
                                    $uibModalInstance.dismiss();
                                }

                                $scope.ok = function () {
                                    closeModal();
                                }
                            }])
                        })
                    }

                    if(attrs.id === 'paypal'){
                        $uibModal.open({
                            template:
                                '<div class="modal-header">' +
                                '<h5 class="modal-title" style="font-weight: bold;font-size: 20px;">More Information</h5>' +
                                '</div>' +
                                '<div class="modal-body"> <p>Integration configuration of Paypal Billing System where clients can make payment securely in just a few clicks and you receive the money in minutes.\n' +
                                '<br><br>Paypal Production\n' +
                                'Enable the Paypal Production Checkbox if you want to use Paypal as billing system.\n' +
                                '<br><br>Paypal Client Secret\n' +
                                '<br>Client Secret is your API credentials, which you use for an access token that authorizes you rest API calls.\n' +
                                'Client Secret is found in Paypal Dashboard at Credentials menu.\n' +
                                '<br><br>Paypal Client ID\n' +
                                '<br>The client ID is unique to the client application on that authorization server.\n' +
                                'Client Secret is found in Paypal Dashboard at Credentials menu.</p>'+

                                '<div class="row">'+
                                '<form>'+
                                '<div class="form-group" style="padding: 20px;">'+
                                '</div>'+
                                '</form>'+
                                '</div>'+
                                '</div>'+
                                '<div class="modal-footer">' +
                                '<button class="btn btn-primary" type="button" ng-click="ok()">OK</button>' +
                                '</div>',

                            controller:('main', ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
                                function closeModal() {
                                    $uibModalInstance.dismiss();
                                }

                                $scope.ok = function () {
                                    closeModal();
                                }
                            }])
                        })
                    }
                    if(attrs.id === 'flussonic'){
                        $uibModal.open({
                            template:
                                '<div class="modal-header">' +
                                '<h5 class="modal-title" style="font-weight: bold;font-size: 20px;">More Information</h5>' +
                                '</div>' +
                                '<div class="modal-body"> <p>Integration configuration for token stream protection via Flussonic.\n' +
                                'Flussonic generates a token string (using a stream name and (optionally) the client\'s IP address) and hashes it by using the same secret key.\n' +
                                '<br><br>Flussonic Key\n' +
                                '<br>Key value used when configuring the token on Flussonic server.\n' +
                                'The license key is stored in the file /etc/flussonic/license.txt.\n' +
                                '<br><br>Flussonic Salt\n' +
                                '<br>Salt value used when configuring the token on Flussonic server\n' +
                                '<br><br>Flussonic Password\n' +
                                '<br>Password used when configuring the token on Flussonic server.\n' +
                                '<br><br>Flussonic Window\n' +
                                '<br>Window value which determines the time in second the token is valuable.</p>'+

                                '<div class="row">'+
                                '<form>'+
                                '<div class="form-group" style="padding: 20px;">'+
                                '</div>'+
                                '</form>'+
                                '</div>'+
                                '</div>'+
                                '<div class="modal-footer">' +
                                '<button class="btn btn-primary" type="button" ng-click="ok()">OK</button>' +
                                '</div>',

                            controller:('main', ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
                                function closeModal() {
                                    $uibModalInstance.dismiss();
                                }

                                $scope.ok = function () {
                                    closeModal();
                                }
                            }])
                        })
                    }

                    if(attrs.id === 'akamai'){
                        $uibModal.open({
                            template:
                                '<div class="modal-header">' +
                                '<h5 class="modal-title" style="font-weight: bold;font-size: 20px;">More Information</h5>' +
                                '</div>' +
                                '<div class="modal-body"> <p>Integration configuration for token stream protection via Akamai.\n' +
                                'Web Application Protector allows you to deploy your security configurations in just a few clicks to protect your applications faster.\n' +
                                '<br><br>Akamai Key\n' +
                                'value is used when configuring the token on Akamai server.\n' +
                                'Akamai Key is found in Akamai Dashboard at System > Settings.\n' +
                                '<br><br>Akamai Window\n' +
                                'value which determines the time in second the token is valuable.\n' +
                                '<br><br>Akamai Salt\n' +
                                'is used when configuring the token on Akamai server.</p>'+

                                '<div class="row">'+
                                '<form>'+
                                '<div class="form-group" style="padding: 20px;">'+
                                '</div>'+
                                '</form>'+
                                '</div>'+
                                '</div>'+
                                '<div class="modal-footer">' +
                                '<button class="btn btn-primary" type="button" ng-click="ok()">OK</button>' +
                                '</div>',

                            controller:('main', ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
                                function closeModal() {
                                    $uibModalInstance.dismiss();
                                }

                                $scope.ok = function () {
                                    closeModal();
                                }
                            }])
                        })
                    }

                    if(attrs.id === 'akamai_segment_media'){
                        $uibModal.open({
                            template:
                                '<div class="modal-header">' +
                                '<h5 class="modal-title" style="font-weight: bold;font-size: 20px;">More Information</h5>' +
                                '</div>' +
                                '<div class="modal-body"> <p>Integration configuration for Akamai Segment Media Token.\n' +
                                '<br>Web Application Protector allows you to deploy your security configurations in just a few clicks to protect your applications faster.\n' +
                                '<br><br>Akamai Key\n' +
                                '<br>Akamai Key is Akamai Segment Media Token to access the integration.\n' +
                                '<br><br>Akamai Window\n' +
                                '<br>Window value which determines the time in second the token is valuable.\n' +
                                '<br><br>Akamai Salt\n' +
                                '<br>Salt used when configuring the token on Akamai server.</p>'+

                                '<div class="row">'+
                                '<form>'+
                                '<div class="form-group" style="padding: 20px;">'+
                                '</div>'+
                                '</form>'+
                                '</div>'+
                                '</div>'+
                                '<div class="modal-footer">' +
                                '<button class="btn btn-primary" type="button" ng-click="ok()">OK</button>' +
                                '</div>',

                            controller:('main', ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
                                function closeModal() {
                                    $uibModalInstance.dismiss();
                                }

                                $scope.ok = function () {
                                    closeModal();
                                }
                            }])
                        })
                    }

                    if(attrs.id === 'verizon'){
                        $uibModal.open({
                            template:
                                '<div class="modal-header">' +
                                '<h5 class="modal-title" style="font-weight: bold;font-size: 20px;">More Information</h5>' +
                                '</div>' +
                                '<div class="modal-body"> <p>Integration configuration for Verizon CDN token creation.\n' +
                                '<br><br>Verizon Key\n' +
                                '<br>Key value used when configuring the token on Verizon server.\n' +
                                '<br><br>Verizon Protocol Allowed\n' +
                                '<br>Verizon Protocol Allowed value specifies the protocol.\n' +
                                '<br><br>Verizon Window\n' +
                                '<br>Window value which determines the time in seconds the token is valuable.</p>'+
                                '<div class="row">'+
                                '<form>'+
                                '<div class="form-group" style="padding: 20px;">'+
                                '</div>'+
                                '</form>'+
                                '</div>'+
                                '</div>'+
                                '<div class="modal-footer">' +
                                '<button class="btn btn-primary" type="button" ng-click="ok()">OK</button>' +
                                '</div>',

                            controller:('main', ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
                                function closeModal() {
                                    $uibModalInstance.dismiss();
                                }

                                $scope.ok = function () {
                                    closeModal();
                                }
                            }])
                        })
                    }

                    if(attrs.id === 'nimble_drm'){
                        $uibModal.open({
                            template:
                                '<div class="modal-header">' +
                                '<h5 class="modal-title" style="font-weight: bold;font-size: 20px;">More Information</h5>' +
                                '</div>' +
                                '<div class="modal-body"> <p>Integration configuration for HLS AES-128 Encryption Key.\n' +
                                '<br>HLS Streaming ( HTTP Live Streaming) is a streaming protocol used for video content across desktop and mobile devices.\n' +
                                'HLS streaming and HLS Encryption can be used for both the cases of live streaming and for Video on Demand streaming (VOD).\n' +
                                '<br><br>HLS AES-128 Encryption Key\n' +
                                '<br>This means media segments are completely encrypted using the Advanced Encryption Standard with a 128-bit key. It also allows for the usage of initialization vectors to optimize the protection.</p>'+
                                '<div class="row">'+
                                '<form>'+
                                '<div class="form-group" style="padding: 20px;">'+
                                '</div>'+
                                '</form>'+
                                '</div>'+
                                '</div>'+
                                '<div class="modal-footer">' +
                                '<button class="btn btn-primary" type="button" ng-click="ok()">OK</button>' +
                                '</div>',

                            controller:('main', ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
                                function closeModal() {
                                    $uibModalInstance.dismiss();
                                }

                                $scope.ok = function () {
                                    closeModal();
                                }
                            }])
                        })
                    }

                    if(attrs.id === 'nimble_token'){
                        $uibModal.open({
                            template:
                                '<div class="modal-header">' +
                                '<h5 class="modal-title" style="font-weight: bold;font-size: 20px;">More Information</h5>' +
                                '</div>' +
                                '<div class="modal-body"> <p>Integration configuration for DRM Key Stream Protection via Nimble.\n' +
                                '<br>Nimble Streamer provides wide feature set for live streaming via various protocols.\n' +
                                'This includes both re-packaging content between protocols and transcoding to change the content itself.\n' +
                                '<br><br>Nimble Token Key\n' +
                                '<br>Key is used to generate token.\n' +
                                '<br><br>Nimble Token Window\n' +
                                '<br>Window defines the time the token is valid.</p>'+
                                '<div class="row">'+
                                '<form>'+
                                '<div class="form-group" style="padding: 20px;">'+
                                '</div>'+
                                '</form>'+
                                '</div>'+
                                '</div>'+
                                '<div class="modal-footer">' +
                                '<button class="btn btn-primary" type="button" ng-click="ok()">OK</button>' +
                                '</div>',

                            controller:('main', ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
                                function closeModal() {
                                    $uibModalInstance.dismiss();
                                }

                                $scope.ok = function () {
                                    closeModal();
                                }
                            }])
                        })
                    }
                    if(attrs.id === 'wowza'){
                        $uibModal.open({
                            template:
                                '<div class="modal-header">' +
                                '<h5 class="modal-title" style="font-weight: bold;font-size: 20px;">More Information</h5>' +
                                '</div>' +
                                '<div class="modal-body"> <p>Integration configuration for stream token generation for WOWZA.\n' +
                                '<br>Wowza\'s full-service video streaming platform delivers reliable, flexible, and scalable solutions for any use case or industry.\n' +
                                '<br><br>Wowza Shared Secret\n' +
                                '<br>Shared Secret value used when configuring the token on Wowza server\n' +
                                '<br><br>Wowza Window\n' +
                                '<br>Window value which determines the time in second the token is valuable.</p>'+
                                '<div class="row">'+
                                '<form>'+
                                '<div class="form-group" style="padding: 20px;">'+
                                '</div>'+
                                '</form>'+
                                '</div>'+
                                '</div>'+
                                '<div class="modal-footer">' +
                                '<button class="btn btn-primary" type="button" ng-click="ok()">OK</button>' +
                                '</div>',

                            controller:('main', ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
                                function closeModal() {
                                    $uibModalInstance.dismiss();
                                }

                                $scope.ok = function () {
                                    closeModal();
                                }
                            }])
                        })
                    }

                    if(attrs.id === 'ezdrm'){
                        $uibModal.open({
                            template:
                                '<div class="modal-header">' +
                                '<h5 class="modal-title" style="font-weight: bold;font-size: 20px;">More Information</h5>' +
                                '</div>' +
                                '<div class="modal-body"> <p>Integration configuration for stream token generation for EZDRM.\n' +
                                '<br>EZDRM is the original specialist in Digital Rights Management as a Service (DRMaaS), offering a straightforward, one-stop solution for protecting and monetizing your video content.\n' +
                                '<br><br>Username\n' +
                                '<br>Parameter username is part of credentials of your account at EZDRM.\n' +
                                '<br><br>Password\n' +
                                '<br>Parameter password is part of credentials of your account at EZDRM.</p>'+
                                '<div class="row">'+
                                '<form>'+
                                '<div class="form-group" style="padding: 20px;">'+
                                '</div>'+
                                '</form>'+
                                '</div>'+
                                '</div>'+
                                '<div class="modal-footer">' +
                                '<button class="btn btn-primary" type="button" ng-click="ok()">OK</button>' +
                                '</div>',

                            controller:('main', ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
                                function closeModal() {
                                    $uibModalInstance.dismiss();
                                }

                                $scope.ok = function () {
                                    closeModal();
                                }
                            }])
                        })
                    }
                    if(attrs.id === 'upgrade_policy'){
                        $uibModal.open({
                            template:
                                '<div class="modal-header">' +
                                '<h5 class="modal-title" style="font-weight: bold;font-size: 20px;">More Information</h5>' +
                                '</div>' +
                                '<div class="modal-body"> <p>Configuration for Device Upgrade Process.\n' +
                                '<br><br>Availability Interval\n' +
                                '<br>This interval includes the availability interval of device upgrade process [[3, 0], [6, 0]] is set as default.\n' +
                                '<br>Availability Denominator is important to find the point of starting an upgrade process.</p>'+
                                '<div class="row">'+
                                '<form>'+
                                '<div class="form-group" style="padding: 20px;">'+
                                '</div>'+
                                '</form>'+
                                '</div>'+
                                '</div>'+
                                '<div class="modal-footer">' +
                                '<button class="btn btn-primary" type="button" ng-click="ok()">OK</button>' +
                                '</div>',

                            controller:('main', ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
                                function closeModal() {
                                    $uibModalInstance.dismiss();
                                }

                                $scope.ok = function () {
                                    closeModal();
                                }
                            }])
                        })
                    }
                    if(attrs.id === 'default_player'){
                        $uibModal.open({
                            template:
                                '<div class="modal-header">' +
                                '<h5 class="modal-title" style="font-weight: bold;font-size: 20px;">More Information</h5>' +
                                '</div>' +
                                '<div class="modal-body"> <p>Default Media Player is a multimedia player that is for everyone, completely free in a very slim and easy to use design.\n' +
                                '<br>Media player can play audio, video files and can run on all android devices.\n' +
                                '<br>ExoPlayer is a media playback library for Android which provides an alternative to Android’s Media Player API.</p>'+
                                '<div class="row">'+
                                '<form>'+
                                '<div class="form-group" style="padding: 20px;">'+
                                '</div>'+
                                '</form>'+
                                '</div>'+
                                '</div>'+
                                '<div class="modal-footer">' +
                                '<button class="btn btn-primary" type="button" ng-click="ok()">OK</button>' +
                                '</div>',

                            controller:('main', ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
                                function closeModal() {
                                    $uibModalInstance.dismiss();
                                }

                                $scope.ok = function () {
                                    closeModal();
                                }
                            }])
                        })
                    }

                    if(attrs.id === 'client_app'){
                        $uibModal.open({
                            template:
                                '<div class="modal-header">' +
                                '<h5 class="modal-title" style="font-weight: bold;font-size: 20px;">More Information</h5>' +
                                '</div>' +
                                '<div class="modal-body"> <p>Configuration for client apps, when you don\'t need no more that app version you put in that field.\n' +
                                '\n' +
                                '<br><br>Blacklist app versions\n' +
                                '<br>Fulfill with an older version of the app that is not in use anymore.</p>'+
                                '<div class="row">'+
                                '<form>'+
                                '<div class="form-group" style="padding: 20px;">'+
                                '</div>'+
                                '</form>'+
                                '</div>'+
                                '</div>'+
                                '<div class="modal-footer">' +
                                '<button class="btn btn-primary" type="button" ng-click="ok()">OK</button>' +
                                '</div>',

                            controller:('main', ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
                                function closeModal() {
                                    $uibModalInstance.dismiss();
                                }

                                $scope.ok = function () {
                                    closeModal();
                                }
                            }])
                        })
                    }

                    if(attrs.id === 'public_api'){
                        $uibModal.open({
                            template:
                                '<div class="modal-header">' +
                                '<h5 class="modal-title" style="font-weight: bold;font-size: 20px;">More Information</h5>' +
                                '</div>' +
                                '<div class="modal-body"> <p>Public API Auth field is used to access public API in Management System.\n' +
                                '<br>Fill Public API Auth form with your public IP.\n' +
                                '<br>If you want to set more than one IP separate them with comma like (127.0.0.1,109.69.2.120)</p>'+
                                '<div class="row">'+
                                '<form>'+
                                '<div class="form-group" style="padding: 20px;">'+
                                '</div>'+
                                '</form>'+
                                '</div>'+
                                '</div>'+
                                '<div class="modal-footer">' +
                                '<button class="btn btn-primary" type="button" ng-click="ok()">OK</button>' +
                                '</div>',

                            controller:('main', ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
                                function closeModal() {
                                    $uibModalInstance.dismiss();
                                }

                                $scope.ok = function () {
                                    closeModal();
                                }
                            }])
                        })
                    }
                    if(attrs.id === 'vmx'){
                        $uibModal.open({
                            template:
                                '<div class="modal-header">' +
                                '<h5 class="modal-title" style="font-weight: bold;font-size: 20px;">More Information</h5>' +
                                '</div>' +
                                '<div class="modal-body"> <p>Verimatrix a global provider of security and analytics solutions that protect content, applications, and devices across multiple markets.\n' +
                                '<br>It offers Android and iOS Application Protection.\n' +
                                '<br>Tenant ID is a globally unique identifier (GUID) that is different than your tenant name or domain.\n' +
                                '<br>Company Name, Username and Password are credentials of you account at Verimatrix.</p>'+
                                '<div class="row">'+
                                '<form>'+
                                '<div class="form-group" style="padding: 20px;">'+
                                '</div>'+
                                '</form>'+
                                '</div>'+
                                '</div>'+
                                '<div class="modal-footer">' +
                                '<button class="btn btn-primary" type="button" ng-click="ok()">OK</button>' +
                                '</div>',

                            controller:('main', ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
                                function closeModal() {
                                    $uibModalInstance.dismiss();
                                }

                                $scope.ok = function () {
                                    closeModal();
                                }
                            }])
                        })
                    }
                    if(attrs.id === 'widevine'){
                        $uibModal.open({
                            template:
                                '<div class="modal-header">' +
                                '<h5 class="modal-title" style="font-weight: bold;font-size: 20px;">More Information</h5>' +
                                '</div>' +
                                '<div class="modal-body"> <p>Google Widevine is a digital rights management (DRM) service used to help, protect online content from piracy. It’s used by a wide variety of popular streaming services to prevent users from downloading or copying their TV shows and movies.\n' +
                                'Widevine’s DRM solution provides the capability to license, securely distribute and protect playback of content on any consumer device.\n' +
                                '\n' +
                                '<br><br>Widevine Key Server enables encryption and decryption. This generates protection systems.\n' +
                                '<br><br>IV Signing iv in hex string. Enables encryption and decryption for protection systems.\n' +
                                '<br><br>Provider is the name of the signer.\n' +
                                '<br><br>License Content Key is Key Server URL required for Widevine encryption and decryption.</p>'+
                                '<div class="row">'+
                                '<form>'+
                                '<div class="form-group" style="padding: 20px;">'+
                                '</div>'+
                                '</form>'+
                                '</div>'+
                                '</div>'+
                                '<div class="modal-footer">' +
                                '<button class="btn btn-primary" type="button" ng-click="ok()">OK</button>' +
                                '</div>',

                            controller:('main', ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
                                function closeModal() {
                                    $uibModalInstance.dismiss();
                                }

                                $scope.ok = function () {
                                    closeModal();
                                }
                            }])
                        })
                    }

                    if(attrs.id === 'elastic_stack'){
                        $uibModal.open({
                            template:
                                '<div class="modal-header">' +
                                '<h5 class="modal-title" style="font-weight: bold;font-size: 20px;">More Information</h5>' +
                                '</div>' +
                                '<div class="modal-body"> <p>Elastic Stack is a group of open source products from Elastic designed to help users take data from any type of source and in any format and search, analyze, ' +
                                'and visualize that data in real time.<br><br>"ELK" Elasticsearch, Logstash, and Kibana. ' +
                                '<br>Elasticsearch is a search and analytics engine. <br>Logstash is a server‑side data processing pipeline that ingests data from multiple sources simultaneously, transforms it, and then sends ' +
                                'it to a "stash" like Elasticsearch. <br>Kibana lets users visualize data with charts and graphs in Elasticsearch.\n' +
                                '<br>Parameters username and password are credentials of your account at Elastic Stack.</p>'+
                                '<div class="row">'+
                                '<form>'+
                                '<div class="form-group" style="padding: 20px;">'+
                                '</div>'+
                                '</form>'+
                                '</div>'+
                                '</div>'+
                                '<div class="modal-footer">' +
                                '<button class="btn btn-primary" type="button" ng-click="ok()">OK</button>' +
                                '</div>',

                            controller:('main', ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
                                function closeModal() {
                                    $uibModalInstance.dismiss();
                                }

                                $scope.ok = function () {
                                    closeModal();
                                }
                            }])
                        })
                    }

                    if(attrs.id === 'google_cloud_storage'){
                        $uibModal.open({
                            template:
                                '<div class="modal-header">' +
                                '<h5 class="modal-title" style="font-weight: bold;font-size: 20px;">More Information</h5>' +
                                '</div>' +
                                '<div class="modal-body"> <p>Cloud Storage is a service for storing your objects in Google Cloud.\n' +
                                '<br>You can store all the files uploaded in the Management System like images, epg_files, videos, apk, subtitles.\n' +
                                'Once you upload your objects to Cloud Storage, you have fine-grained control over how you secure and share your data.\n' +
                                '\n' +
                                '<br><br>Google Managed Key is your Encryption Key generated at Service Account at Google Cloud Storage.\n' +
                                '<br>Project ID is your ID of project created at your Storage Browser.\n' +
                                '<br>Bucket Name is unique, is the name of the bucket created in AWS Region.</p>'+
                                '<div class="row">'+
                                '<form>'+
                                '<div class="form-group" style="padding: 20px;">'+
                                '</div>'+
                                '</form>'+
                                '</div>'+
                                '</div>'+
                                '<div class="modal-footer">' +
                                '<button class="btn btn-primary" type="button" ng-click="ok()">OK</button>' +
                                '</div>',

                            controller:('main', ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
                                function closeModal() {
                                    $uibModalInstance.dismiss();
                                }

                                $scope.ok = function () {
                                    closeModal();
                                }
                            }])
                        })
                    }
                    if(attrs.id === 'concurrent_logins'){
                        $uibModal.open({
                            template:
                                '<div class="modal-header">' +
                                '<h5 class="modal-title" style="font-weight: bold;font-size: 20px;">More Information</h5>' +
                                '</div>' +
                                '<div class="modal-body"> <p>Concurrent login number is the maximum number of people who can use the same account at the same time.</p>'+
                                '<div class="row">'+
                                '<form>'+
                                '<div class="form-group" style="padding: 20px;">'+
                                '</div>'+
                                '</form>'+
                                '</div>'+
                                '</div>'+
                                '<div class="modal-footer">' +
                                '<button class="btn btn-primary" type="button" ng-click="ok()">OK</button>' +
                                '</div>',

                            controller:('main', ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
                                function closeModal() {
                                    $uibModalInstance.dismiss();
                                }

                                $scope.ok = function () {
                                    closeModal();
                                }
                            }])
                        })
                    }
                    if(attrs.id === 'google_analytics'){
                        $uibModal.open({
                            template:
                                '<div class="modal-header">' +
                                '<h5 class="modal-title" style="font-weight: bold;font-size: 20px;">More Information</h5>' +
                                '</div>' +
                                '<div class="modal-body"> <p>Google analytics tracer to monitor audience and system logs, to help you analyze your website traffic.\n' +
                                '\n' +
                                '<br><br>Google Analytics Tracking ID is the unique identifier of a Google Analytics property.\n' +
                                '<br>Google Analytics Traces ID is found in Analytics Admin Page > Property Settings > Tracing Code.</p>'+
                                '<div class="row">'+
                                '<form>'+
                                '<div class="form-group" style="padding: 20px;">'+
                                '</div>'+
                                '</form>'+
                                '</div>'+
                                '</div>'+
                                '<div class="modal-footer">' +
                                '<button class="btn btn-primary" type="button" ng-click="ok()">OK</button>' +
                                '</div>',

                            controller:('main', ['$scope', '$uibModalInstance', function ($scope, $uibModalInstance) {
                                function closeModal() {
                                    $uibModalInstance.dismiss();
                                }

                                $scope.ok = function () {
                                    closeModal();
                                }
                            }])
                        })
                    }

                }

        },
        template: '<a class="btn btn-info btn-md btn-circle" ng-click="modal()" style="font-weight: bold"><span class="{{ icon }}" aria-hidden="true"></span>&nbsp;{{ label }}</a>'
    };
}

integrationInfo.$inject = ['Restangular', '$uibModal', '$q', 'notification', '$state','$http'];

export default integrationInfo;