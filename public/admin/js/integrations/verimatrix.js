import Template from './verimatrix.html';

function integrate($stateProvider) {

    $stateProvider.state('verimatrix', {
        parent: 'main',
        url: '/configs/integration/verimatrix',
        headers: { "Content-Type": "application/json;charset=UTF-8" },
        controller: ['$http', '$scope', 'notification', function($http, $scope, notification) {

            const token = localStorage.userToken || "";

            let integrationKey ;
            //get key of config
            $scope.getIntegrationKey = function ()
            {
                let findKey = angular.element(document.querySelector( '#vmx' ) );
                integrationKey = findKey[0].id;
                const config = {
                    method: 'GET',
                    url: '../api/configs/integration/'+integrationKey,
                    headers: {'Authorization': token},
                    data: {integrationKey: 'integrationKey'}
                };

                $http(config).then(function(response) {
                    $scope.model = [];
                    $scope.model = {
                        tenant_id : response.data.value.tenant_id,
                        company_name : response.data.value.company_name,
                        username : response.data.value.username,
                        password : response.data.value.password,
                        description : response.data.description,
                    }

                    $scope.updateVMX = function () {
                        let tenant_id = document.getElementById('tenant_id').value;
                        let company_name = document.getElementById('companyName').value;
                        let username = document.getElementById('username').value;
                        let password = document.getElementById('password').value;

                        let data = { value: {
                                "tenant_id" : tenant_id,
                                "company_name" : company_name,
                                "username" : username,
                                "password" : password,
                            } };

                        const integrationId = response.data.id
                        const config = {
                            method: 'POST',
                            url: '../api/configs/'+integrationId,
                            headers: {'Authorization': token},
                            data: data
                        };

                        $http(config).then(function successCallback(response) {
                            if (response.status === 200) {
                                notification.log('Update Successfully', {addnCls: 'humane-flatty-success'});
                                window.location.href = ".#/integrations";
                            }
                        }, function errorCallback(response) {
                            if (response.status === 400) {
                                notification.log(response.data.message, {addnCls: 'humane-flatty-error'});
                            } else {
                                notification.log(response.data.message, {addnCls: 'humane-flatty-error'});
                            }
                        });
                    }
                })
            };
        }],
        template: Template
    });
}

integrate.$inject = ['$stateProvider'];

export default integrate;