import Template from './woo_commerce.html';

function integrate($stateProvider) {

    $stateProvider.state('woo_commerce', {
        parent: 'main',
        url: '/configs/integration/woo_commerce',
        headers: { "Content-Type": "application/json;charset=UTF-8" },
        controller: ['$http', '$scope', 'notification', function($http, $scope, notification) {

            const token = localStorage.userToken || "";

            let integrationKey ;
            //get key of config
            $scope.getIntegrationKey = function ()
            {
                let findKey = angular.element(document.querySelector( '#woocomerce' ) );
                integrationKey = findKey[0].id;

                const config = {
                    method: 'GET',
                    url: '../api/configs/integration/'+integrationKey,
                    headers: {'Authorization': token},
                    data: {integrationKey: 'integrationKey'}
                };

                $http(config).then(function(response) {
                    $scope.model = [];
                    $scope.model = {
                        base_url : response.data.value.base_url,
                        consumer_key : response.data.value.consumer_key,
                        consumer_secret : response.data.value.consumer_secret,
                        description : response.data.description,

                    }

                    $scope.updateWooCommerce = function () {
                        let base_url = document.getElementById('base_url').value;
                        let consumer_key = document.getElementById('consumer_key').value;
                        let consumer_secret = document.getElementById('consumer_secret').value;
                        let data = { value: {
                                "base_url" : base_url,
                                "consumer_key" : consumer_key,
                                "consumer_secret" : consumer_secret,
                            } };

                        const integrationId = response.data.id
                        const config = {
                            method: 'POST',
                            url: '../api/configs/'+integrationId,
                            headers: {'Authorization': token},
                            data: data
                        };

                        $http(config).then(function successCallback(response) {
                            if (response.status === 200) {
                                notification.log('Update Successfully', {addnCls: 'humane-flatty-success'});
                                window.location.href = ".#/integrations";
                            }
                        }, function errorCallback(response) {
                            if (response.status === 400) {
                                notification.log(response.data.message, {addnCls: 'humane-flatty-error'});
                            } else {
                                notification.log(response.data.message, {addnCls: 'humane-flatty-error'});
                            }
                        });
                    }
                })
            };
        }],
        template: Template
    });
}

integrate.$inject = ['$stateProvider'];

export default integrate;