import Template from './stripe.html';

function integrate($stateProvider) {

    $stateProvider.state('stripe', {
        parent: 'main',
        url: '/configs/integration/stripe',
        headers: { "Content-Type": "application/json;charset=UTF-8" },
        controller: ['$http', '$scope', 'notification', function($http, $scope, notification) {

            const token = localStorage.userToken || "";

            let integrationKey ;
            //get key of config
            $scope.getIntegrationKey = function ()
            {
                let findKey = angular.element(document.querySelector( '#stripe' ) );
                integrationKey = findKey[0].id;
                const config = {
                    method: 'GET',
                    url: '../api/configs/integration/'+integrationKey,
                    headers: {'Authorization': token},
                    data: {integrationKey: 'integrationKey'}
                };

                $http(config).then(function(response) {
                    $scope.model = [];
                    $scope.model = {
                        api_key : response.data.value.api_key,
                        publishable_key : response.data.value.publishable_key,
                        secret_key : response.data.value.secret_key,
                        description : response.data.description,

                    }

                    $scope.updateStripe = function () {
                        let api_key = document.getElementById('api_key').value;
                        let publishable_key = document.getElementById('publishable_key').value;
                        let secret_key = document.getElementById('secret_key').value;
                        let data = { value: {
                                "api_key" : api_key,
                                "publishable_key" : publishable_key,
                                "secret_key" : secret_key
                            } };

                        const integrationId = response.data.id
                        const config = {
                            method: 'POST',
                            url: '../api/configs/'+integrationId,
                            headers: {'Authorization': token},
                            data: data
                        };

                        $http(config).then(function successCallback(response) {
                            if (response.status === 200) {
                                notification.log('Update Successfully', {addnCls: 'humane-flatty-success'});
                                window.location.href = ".#/integrations";
                            }
                        }, function errorCallback(response) {
                            if (response.status === 400) {
                                notification.log(response.data.message, {addnCls: 'humane-flatty-error'});
                            } else {
                                notification.log(response.data.message, {addnCls: 'humane-flatty-error'});
                            }
                        });
                    }
                })
            };
        }],
        template: Template
    });
}

integrate.$inject = ['$stateProvider'];

export default integrate;