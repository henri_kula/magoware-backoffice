import Template from './google_storage.html';

function integrate($stateProvider) {

    $stateProvider.state('google_storage', {
        parent: 'main',
        url: '/configs/integration/google_storage',
        headers: { "Content-Type": "application/json;charset=UTF-8" },
        controller: ['$http', '$scope', 'notification', function($http, $scope, notification) {

            const token = localStorage.userToken || "";

            let integrationKey ;
            //get key of config
            $scope.getIntegrationKey = function ()
            {
                let findKey = angular.element(document.querySelector( '#google_cloud_storage' ) );
                integrationKey = findKey[0].id;
                const config = {
                    method: 'GET',
                    url: '../api/configs/integration/'+integrationKey,
                    headers: {'Authorization': token},
                    data: {integrationKey: 'integrationKey'}
                };

                $http(config).then(function(response) {
                    $scope.model = [];
                    $scope.model = {
                        storage : response.data.value.storage,
                        google_managed_key : response.data.value.google_managed_key,
                        projectId : response.data.value.projectId,
                        bucket_name : response.data.value.bucket_name,
                        description : response.data.description
                    }

                    $scope.setValue = function(thevalue){
                        $scope.model.storage= thevalue
                    }

                    $scope.updateGoogleStorage = function () {
                        let storage = $scope.model.storage;
                        let google_managed_key = document.getElementById('google_managed_key').value;
                        let projectId = document.getElementById('projectId').value;
                        let bucket_name = document.getElementById('bucket_name').value;

                        let data = { value: {
                                "storage" : storage,
                                "google_managed_key" : google_managed_key,
                                "projectId" : projectId,
                                "bucket_name" : bucket_name
                            } };

                        const integrationId = response.data.id
                        const config = {
                            method: 'POST',
                            url: '../api/configs/'+integrationId,
                            headers: {'Authorization': token},
                            data: data
                        };

                        $http(config).then(function successCallback(response) {
                            if (response.status === 200) {
                                notification.log('Update Successfully', {addnCls: 'humane-flatty-success'});
                                window.location.href = ".#/integrations";
                            }
                        }, function errorCallback(response) {
                            if (response.status === 400) {
                                notification.log(response.data.message, {addnCls: 'humane-flatty-error'});
                            } else {
                                notification.log(response.data.message, {addnCls: 'humane-flatty-error'});
                            }
                        });
                    }
                })
            };
        }],
        template: Template
    });
}

integrate.$inject = ['$stateProvider'];

export default integrate;