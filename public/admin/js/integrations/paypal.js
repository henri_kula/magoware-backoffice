import Template from './paypal.html';

function integrate($stateProvider) {

    $stateProvider.state('paypal', {
        parent: 'main',
        url: '/configs/integration/paypal',
        headers: { "Content-Type": "application/json;charset=UTF-8" },
        controller: ['$http', '$scope', 'notification', function($http, $scope, notification) {

            const token = localStorage.userToken || "";

            let integrationKey ;
            //get key of config
            $scope.getIntegrationKey = function ()
            {
                let findKey = angular.element(document.querySelector( '#paypal' ) );
                integrationKey = findKey[0].id;
                const config = {
                    method: 'GET',
                    url: '../api/configs/integration/'+integrationKey,
                    headers: {'Authorization': token},
                    data: {integrationKey: 'integrationKey'}
                };

                $http(config).then(function(response) {
                    $scope.model = [];
                    $scope.model = {
                        production : response.data.value.production,
                        client_id : response.data.value.client_id,
                        client_secret : response.data.value.client_secret,
                        description : response.data.description
                    }

                    $scope.setValue = function(thevalue){
                        $scope.model.production = thevalue;
                    }

                    $scope.updatePaypal = function () {
                        let production = $scope.model.production;
                        let client_id = document.getElementById('client_id').value;
                        let client_secret = document.getElementById('client_secret').value;

                        let data = { value: {
                                "production" : production,
                                "client_id" : client_id,
                                "client_secret" : client_secret
                            } };

                        const integrationId = response.data.id
                        const config = {
                            method: 'POST',
                            url: '../api/configs/'+integrationId,
                            headers: {'Authorization': token},
                            data: data
                        };

                        $http(config).then(function successCallback(response) {
                            if (response.status === 200) {
                                notification.log('Update Successfully', {addnCls: 'humane-flatty-success'});
                                window.location.href = ".#/integrations";
                            }
                        }, function errorCallback(response) {
                            if (response.status === 400) {
                                notification.log(response.data.message, {addnCls: 'humane-flatty-error'});
                            } else {
                                notification.log(response.data.message, {addnCls: 'humane-flatty-error'});
                            }
                        });
                    }
                })
            };
        }],
        template: Template
    });
}

integrate.$inject = ['$stateProvider'];

export default integrate;