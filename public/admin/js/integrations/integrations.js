import Template from './integrations.html';

function integrations($stateProvider) {
    $stateProvider.state('integrations', {
        parent: 'main',
        url: '/integrations',
        headers: {
            "Content-Type": "application/json;charset=UTF-8"
        },
        controller: ['$http', '$scope', 'notification', function ($http, $scope, notification) {
            const token = localStorage.userToken || "";
            const getConfig = {
                method: 'GET',
                url: '../api/configs',
                headers: {'Authorization': token}
            };


            $http(getConfig).then(function successCallback(response) {
            }, function errorCallback(response) {
                if (response.status === 400) {
                    notification.log(response.data.message, {addnCls: 'humane-flatty-error'});
                } else {
                    notification.log(response.data.message, {addnCls: 'humane-flatty-error'});
                }
            });


        }

        ],
        template: Template
    })
    ;
}

integrations.$inject = ['$stateProvider'];

export default integrations;
