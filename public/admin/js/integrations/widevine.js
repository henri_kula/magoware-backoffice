import Template from './widevine.html';

function integrate($stateProvider) {

    $stateProvider.state('widevine', {
        parent: 'main',
        url: '/configs/integration/widevine',
        headers: { "Content-Type": "application/json;charset=UTF-8" },
        controller: ['$http', '$scope', 'notification', function($http, $scope, notification) {

            const token = localStorage.userToken || "";

            let integrationKey ;
            //get key of config
            $scope.getIntegrationKey = function ()
            {
                let findKey = angular.element(document.querySelector( '#widevine' ) );
                integrationKey = findKey[0].id;
                const config = {
                    method: 'GET',
                    url: '../api/configs/integration/'+integrationKey,
                    headers: {'Authorization': token},
                    data: {integrationKey: 'integrationKey'}
                };

                $http(config).then(function(response) {
                    $scope.model = [];
                    $scope.model = {
                        key : response.data.value.key,
                        iv : response.data.value.iv,
                        provider : response.data.value.provider,
                        license_content_key : response.data.value.license_content_key,
                        description : response.data.description
                    }

                    $scope.updateWidevine = function () {
                        let key = document.getElementById('key').value;
                        let iv = document.getElementById('iv').value;
                        let provider = document.getElementById('provider').value;
                        let license_content_key = document.getElementById('license_content_key').value;

                        let data = { value: {
                                "key" : key,
                                "iv" : iv,
                                "provider" : provider,
                                "license_content_key" : license_content_key
                            } };

                        const integrationId = response.data.id
                        const config = {
                            method: 'POST',
                            url: '../api/configs/'+integrationId,
                            headers: {'Authorization': token},
                            data: data
                        };

                        $http(config).then(function successCallback(response) {
                            if (response.status === 200) {
                                notification.log('Update Successfully', {addnCls: 'humane-flatty-success'});
                                window.location.href = ".#/integrations";
                            }
                        }, function errorCallback(response) {
                            if (response.status === 400) {
                                notification.log(response.data.message, {addnCls: 'humane-flatty-error'});
                            } else {
                                notification.log(response.data.message, {addnCls: 'humane-flatty-error'});
                            }
                        });
                    }
                })
            };
        }],
        template: Template
    });
}

integrate.$inject = ['$stateProvider'];

export default integrate;