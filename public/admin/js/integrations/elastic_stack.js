import Template from './elastic_stack.html';

function integrate($stateProvider) {

    $stateProvider.state('elastic_stack', {
        parent: 'main',
        url: '/configs/integration/elastic_stack',
        headers: { "Content-Type": "application/json;charset=UTF-8" },
        controller: ['$http', '$scope', 'notification', function($http, $scope, notification) {

            const token = localStorage.userToken || "";

            let integrationKey ;
            //get key of config
            $scope.getIntegrationKey = function ()
            {
                let findKey = angular.element(document.querySelector( '#elastic_stack' ) );
                integrationKey = findKey[0].id;
                const config = {
                    method: 'GET',
                    url: '../api/configs/integration/'+integrationKey,
                    headers: {'Authorization': token},
                    data: {integrationKey: 'integrationKey'}
                };

                $http(config).then(function(response) {
                    $scope.model = [];
                    $scope.model = {
                        elastic_stack_id : response.data.value.elastic_stack_id,
                        url : response.data.value.url,
                        username : response.data.value.username,
                        password : response.data.value.password,
                        description : response.data.description
                    }

                    $scope.setValue = function(thevalue){
                        $scope.model.elastic_stack_id = thevalue
                    }

                    $scope.updateElasticStack = function () {
                        let elastic_stack_id = $scope.model.elastic_stack_id;
                        let url = document.getElementById('url').value;
                        let username = document.getElementById('username').value;
                        let password = document.getElementById('password').value;

                        let data = { value: {
                                "elastic_stack_id": elastic_stack_id,
                                "url" : url,
                                "username" : username,
                                "password" : password
                            } };

                        const integrationId = response.data.id
                        const config = {
                            method: 'POST',
                            url: '../api/configs/'+integrationId,
                            headers: {'Authorization': token},
                            data: data
                        };

                        $http(config).then(function successCallback(response) {
                            if (response.status === 200) {
                                notification.log('Update Successfully', {addnCls: 'humane-flatty-success'});
                                window.location.href = ".#/integrations";
                            }
                        }, function errorCallback(response) {
                            if (response.status === 400) {
                                notification.log(response.data.message, {addnCls: 'humane-flatty-error'});
                            } else {
                                notification.log(response.data.message, {addnCls: 'humane-flatty-error'});
                            }
                        });
                    }
                })
            };
        }],
        template: Template
    });
}

integrate.$inject = ['$stateProvider'];

export default integrate;