'use strict'

const winston = require('winston');
const { models } = require('./sequelize');
const redis = require('./redis');

/**
 * In this pub sub channel messages should have following format:
 * {
 *   "company_id": n
 *   "type:": "command | event"
 *   "event_type": "changed" - field is only present for events
 *   "config_key": "",
 * }
 * 
 * command is reserved for use in future when it will be needed to interact with
 * config manager from portal like clearing local / and or redis cache for specific configs
 */
const PUB_SUB_CHANNEL = 'configs';


/**
 * Configs cache is an object which hold local copies of configs locally
 * Key format is company_id:key
 */
var configsCache = {};

/**
 * Get a configuration by key for a specific company.
 * @param {String} key Config key identifier.
 * @param {Number} companyId Company id.
 * @return {Object} config as object.
 */
async function getConfig(key, companyId) {
    const configStoreKey = companyId + ':' + key;

    let config = configsCache[configStoreKey];
    if (config) {
        return config;
    }

    //Config was not found in local cache
    //Try get it from redis
    try {
        winston.info('Getting config from redis, key:' + configStoreKey);

        config = await getConfigFromRedis(configStoreKey);
        configsCache[configStoreKey] = config;

        return config;
    }
    catch (err) {
        //Make sure that the error was not thrown because config was not found
        if (!err.code) {
            winston.error('Redis failed with unexpected error:', err);
        }
    }

    try {
        winston.info('Getting config from db, key:' + key);

        let result = await models.configs.findOne({
            where: {
                company_id: companyId,
                key
            }
        });

        if (!result) {
            throw new Error('Could not find any config with key:' + key);
        }

        config = result.value;
        configsCache[configStoreKey] = config;

        try {
            await storeConfigToRedis(configStoreKey, config);
        }
        catch(err) {
            winston.error('Failed to store config to redis with error:', err)
        }

        return config;
    }
    catch(err) {
        winston.error("Retrieving config from db failed with error:", err);
        throw err;
    }
}

/**
 * Update a config.
 * This method updates the config only to Redis.
 * Notifies others to invalidate app internal state.
 * @param {String} key Key identifier
 * @param {Object} value Config state
 * @param {Number} companyId Company id.
 */
function updateConfig(key, value, companyId) {
    let storeKey = companyId + ':' + key;
    
    if (configsCache[storeKey]) {
        delete configsCache[storeKey];
    }
    

    return storeConfigToRedis(storeKey, value)
        .then(function() {
            let message = {
                company_id: companyId,
                type: 'event',
                event_type: 'update',
                config_key: key
            }

            redis.client.publish(PUB_SUB_CHANNEL, JSON.stringify(message))
        });
}

/**
 * Init config manager.
 */
function initManager() {
    createPubSubChannel();
}

function getConfigFromRedis(key) {
    return new Promise((resolve, reject) => {
        redis.client.get(key, function(err, configText) {
            if (err) {
                reject(err);
                return;
            }

            if (!configText) {
                reject({code: 404});
                return;
            }

            let config = JSON.parse(configText);
            resolve(config);
        });
    })
}

function storeConfigToRedis(key, config) {
    return new Promise(function(resolve, reject) {
        let configText = JSON.stringify(config);

        redis.client.set(key, configText, function(err) {
            if (err) {
                reject(err);
                return;
            }

            resolve();
        });
    });
}

function createPubSubChannel() {
    let subscriber = redis.client.duplicate();
    subscriber.on('message', onMessage);

    subscriber.subscribe(PUB_SUB_CHANNEL);
}

function onMessage(channel, message) {
    if (channel != PUB_SUB_CHANNEL) {
        return;
    }
    
    let messageObject = JSON.parse(message);

    if (messageObject.type == 'event') {
        if (messageObject.event_type == "update") {
            onConfigUpdated(messageObject.config_key, messageObject.company_id);
        }
    }
}

function onConfigUpdated(key, companyId) {
    winston.info('config updated event, key: '+ key + ' company id:' + companyId)
    let storeKey = companyId + ':' + key;

    if (configsCache[storeKey]) {
        delete configsCache[storeKey];
    }
}

module.exports = {
    initManager,
    getConfig,
    updateConfig
}