'use strict'

const path = require('path');
const config = require(path.resolve('./config/env/db.connection'));

/**
 * Get the fk names of column.
 * @param {QueryInterface} queryInterface Query Interface of migration
 * @param {String} tableName Table name
 * @param {String} columnName Column name
 */
function getColumnFK(queryInterface, tableName, columnName) {
    let metadataQuery = `
        SELECT 
          TABLE_NAME,COLUMN_NAME,CONSTRAINT_NAME, REFERENCED_TABLE_NAME,REFERENCED_COLUMN_NAME
        FROM
          INFORMATION_SCHEMA.KEY_COLUMN_USAGE
        WHERE
          REFERENCED_TABLE_SCHEMA = '` + config.database + `' AND
          TABLE_NAME = '` + tableName + `' AND
          COLUMN_NAME = '` + columnName + `';`

    return queryInterface.sequelize.query(metadataQuery)
        .then(function(rows) {
            if (rows.length > 0) {
                let fks = [];

                for (const row of rows[0]) {
                    fks.push(row.CONSTRAINT_NAME);
                }

                return fks;
            }

            return [];
        });
}

module.exports = {
    getColumnFK
}