'use strict'

const { BullAdapter, setQueues } = require('bull-board');
const { getQueues } = require('../../../../config/lib/scheduler');

exports.enableBullBoard = function() {
    let boardQueues = [];
    for (let queue of getQueues()) {
        boardQueues.push(new BullAdapter(queue));
    }

    setQueues(boardQueues);
}