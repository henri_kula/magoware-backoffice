'use strict'

const { models, sequelize } = require('../../../../config/lib/sequelize');
const configManager = require('../../../../config/lib/config_manager');
const winston = require('winston');
const errorHandler = require('../../../core/server/controllers/errors.server.controller');
const { t } = require('tar');
const { fchown } = require('fs-extra');

/**
 * Create
 */
exports.create = async function(req, res) {
    req.body.company_id = req.token.company_id;

    try {
        let configInst = await models.configs.create(req.body);
        res.json(configInst);
    }
    catch(err) {
        winston.error('Creating config failed with error:', err);
        res.status(400).send({
            message: errorHandler.getErrorMessage(err)
        });
    }
};

/**
 * Show current
 */
exports.read = function(req, res) {
    res.json(req.config);
};

/**
 * Update
 */
exports.update = async function(req, res) {
    let t = await sequelize.transaction();

    try {
        let updatedInst = await req.config.update(req.body, {transaction: t});

        await configManager.updateConfig(updatedInst.key, updatedInst.value, req.token.company_id);

        await t.commit();

        res.json(updatedInst);
    }
    catch(err) {
        winston.error("Updating config failed with error:", err);
        await t.rollback();
        return res.status(400).send({
            message: errorHandler.getErrorMessage(err)
        });
    }
};

/**
 * Delete
 */
exports.delete = async function(req, res) {
    try {
        await req.config.destroy();
        res.json(req.config);
    }
    catch(err) {
        winston.error('Deleting config failed with error: ', err);
        res.status(400).send({
            message: errorHandler.getErrorMessage(err)
        });
    }
};

/**
 * List
 */
exports.list = async function(req, res) {
    try {
        let urlQuery = req.query;
        let where = {company_id: req.token.company_id}
        let queryOptions = {
            attributes: ['id', 'company_id', 'key', 'description', 'value'],
            where
        };

        if (urlQuery.category) {
            where.category = urlQuery.category;
        }

        if(parseInt(urlQuery._end) !== -1){
            if(parseInt(urlQuery._start)) queryOptions.offset = parseInt(urlQuery._start);
            if(parseInt(urlQuery._end)) queryOptions.limit = parseInt(urlQuery._end)-parseInt(urlQuery._start);
        }

        let result = await models.configs.findAndCountAll(queryOptions);
        res.setHeader("X-Total-Count", result.count);
        res.json(result.rows);
    }
    catch(err) {
        winston.error("Getting list of configs failed with error: ", err);
        res.status(400).send({
            message: errorHandler.getErrorMessage(err)
        });
    }
};


exports.configsByKey = async function (req, res) {
    try {
        let result = await models.configs.findOne({
            where: {company_id: req.token.company_id, key: req.params.key},
        });

        if (!result) {
            res.status(404).send({
                message: 'No config with that id has been found'
            });
            return;
        }

        res.json(result);
    } catch (err) {
        winston.error("Getting config by ID failed with error:", err);
        res.status(500).send({
            message: errorHandler.getErrorMessage(err)
        });
    }
}


/**
 * middleware
 */
exports.dataByID = async function(req, res, next) {
    let id = parseInt(req.params.id);

    if (!Number.isInteger(id)) {
        res.status(400).send({
            message: 'Id must be a number'
        });

        return;
    }

    try {
        let result = await models.configs.findOne({
            where: {id: id, company_id: req.token.company_id},
        });

        if (!result) {
            res.status(404).send({
                message: 'No config with that id has been found'
            });

            return;
        }

        req.config = result;
        next();
    }
    catch(err) {
        winston.error("Getting config failed with error:",err);
        res.status(500).send({
            message: errorHandler.getErrorMessage(err)
        });
    }
};
