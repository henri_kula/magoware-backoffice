'use strict'

module.exports = function (sequelize, DataTypes) {
    const configModel = sequelize.define('configs', {
        id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
            unique: true
        },
        company_id: {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            defaultValue: 1
        },
        key: {
            type: DataTypes.STRING(20),
            allowNull: false,
        },
        category: {
            type: DataTypes.STRING(20),
            allowNull: false,
        },
        description: {
            type: DataTypes.STRING(255),
            allowNull: false
        },
        value: {
            type: DataTypes.JSON,
            allowNull: false,
        }
    }, {
        tableName: 'configs',
        indexes: [
            {
                unique: true,
                fields: ['company_id', 'key'],
                name: 'unique_company_id_and_key'
            }
        ],
        associate: function (models) {
            configModel.belongsTo(models.settings, { foreignKey: 'company_id' });
        }
    });
    return configModel;
};
