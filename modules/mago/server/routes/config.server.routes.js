'use strict'

const policy = require('../policies/mago.server.policy');
const handler = require('../controllers/configs.server.controller');

module.exports = function(app) {
    app.route('/api/configs')
        .all(policy.Authenticate)
        .all(policy.isAllowed)
        .get(handler.list)
        .post(handler.create);

    app.route('/api/configs/:id')
        .all(policy.Authenticate)
        .all(policy.isAllowed)
        .all(handler.dataByID)
        .get(handler.read)
        .post(handler.update)
        .delete(handler.delete);

    app.route('/api/configs/integration/:key')
        .all(policy.Authenticate)
        .all(policy.isAllowed)
        .get(handler.configsByKey);
}