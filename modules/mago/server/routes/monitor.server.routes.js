'use strict'

const handler = require('../controllers/monitor.server.controller');
const policy = require('../policies/mago.server.policy');
const bullBoard = require('bull-board');

module.exports = function(app) {
    handler.enableBullBoard();

    app.use('/api/bull/board', [policy.requireAuthCookie, bullBoard.router]);
}