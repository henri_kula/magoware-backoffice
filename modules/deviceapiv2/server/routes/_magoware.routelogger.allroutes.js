var logger = require('logzio-nodejs').createLogger({
    token: 'bydnmyPsSYWJyqwVTLKJqhUzjdtexWbA',
    host: 'listener-eu.logz.io',
    type: 'MAGOWARE BACKOFFICE'     // OPTIONAL (If none is set, it will be 'nodejs')
});
const guestRateLimiterMiddleware = require('../../../../config/lib/rate_limiter_redis').guestRateLimiterMiddleware;

module.exports = function(app) {
    app.use('/apiv2/*', function (req, res, next) {
        let logzobject = {};
        logzobject.headers = req.headers;
        logzobject.body = req.body;
        logger.log(logzobject);
        next();
    });

    // rate limiter for guest user
    app.use('/apiv3/guest/*', guestRateLimiterMiddleware);
};