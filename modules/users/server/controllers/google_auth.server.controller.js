'use strict'

const path = require('path'),
    authentication = require(path.resolve('./modules/mago/server/controllers/authentication.controller')),
    companyFn = require(path.resolve('./custom_functions/company')),
    winston = require('winston');


exports.create_company_form = function (req, res) {
    res.render(path.resolve('./modules/users/server/templates/create-company'), { title: 'Create new company', email: req.params.email })
};

exports.create_company_and_user = function (req, res) {
    //set email in body container
    req.body.email = req.params.email;
    
    //set 30 day trial
    let expire_date = new Date(Date.now());
    expire_date.setDate(expire_date.getDate() + 30);
    req.body.expire_date = expire_date.toString();

    companyFn.createCompany(req).then(function (result) {
        req.body.token = authentication.issueJWT(result.owner.id, result.owner.username, 'admin', result.owner.company_id)
        res.redirect('/admin/#/auth?access_token=' + req.body.token)
    }).catch(function (error) {
        winston.error('Company create failed with error: ' + error);
        res.redirect('/admin/')
    });
};

