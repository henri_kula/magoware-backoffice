'use strict'
var path = require('path'),
    url = require('url'),
    response = require(path.resolve("./config/responses.js")),
    akamai_token_generator = require(path.resolve('./modules/streams/server/controllers/akamai_token_v2')),
    crypto = require('crypto'),
    responses = require(path.resolve("./config/responses.js")),
    ectoken = require('ectoken').V3,
    EdgeAuth = require('akamai-edgeauth');

const { getConfig } = require('../../../../config/lib/config_manager');
const responseHandler = require("../../../device-api-v4/server/utils/response");
const winston = require("winston")


//nimble dateFormat function
var dateFormat = function () {
    var	token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
        timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
        timezoneClip = /[^-+\dA-Z]/g,
        pad = function (val, len) {
            val = String(val);
            len = len || 2;
            while (val.length < len) val = "0" + val;
            return val;
        };

    // Regexes and supporting functions are cached through closure
    return function (date, mask, utc) {
        var dF = dateFormat;

        // You can't provide utc if you skip other args (use the "UTC:" mask prefix)
        if (arguments.length == 1 && Object.prototype.toString.call(date) == "[object String]" && !/\d/.test(date)) {
            mask = date;
            date = undefined;
        }

        // Passing date through Date applies Date.parse, if necessary
        date = date ? new Date(date) : new Date;
        if (isNaN(date)) throw SyntaxError("invalid date");

        mask = String(dF.masks[mask] || mask || dF.masks["default"]);

        // Allow setting the utc argument via the mask
        if (mask.slice(0, 4) == "UTC:") {
            mask = mask.slice(4);
            utc = true;
        }

        var	_ = utc ? "getUTC" : "get",
            d = date[_ + "Date"](),
            D = date[_ + "Day"](),
            m = date[_ + "Month"](),
            y = date[_ + "FullYear"](),
            H = date[_ + "Hours"](),
            M = date[_ + "Minutes"](),
            s = date[_ + "Seconds"](),
            L = date[_ + "Milliseconds"](),
            o = utc ? 0 : date.getTimezoneOffset(),
            flags = {
                d:    d,
                dd:   pad(d),
                ddd:  dF.i18n.dayNames[D],
                dddd: dF.i18n.dayNames[D + 7],
                m:    m + 1,
                mm:   pad(m + 1),
                mmm:  dF.i18n.monthNames[m],
                mmmm: dF.i18n.monthNames[m + 12],
                yy:   String(y).slice(2),
                yyyy: y,
                h:    H % 12 || 12,
                hh:   pad(H % 12 || 12),
                H:    H,
                HH:   pad(H),
                M:    M,
                MM:   pad(M),
                s:    s,
                ss:   pad(s),
                l:    pad(L, 3),
                L:    pad(L > 99 ? Math.round(L / 10) : L),
                t:    H < 12 ? "a"  : "p",
                tt:   H < 12 ? "am" : "pm",
                T:    H < 12 ? "A"  : "P",
                TT:   H < 12 ? "AM" : "PM",
                Z:    utc ? "UTC" : (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
                o:    (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
                S:    ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
            };

        return mask.replace(token, function ($0) {
            return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
        });
    };
}();

// Nimble dateFormat MASKS
dateFormat.masks = {
    "default":      "ddd mmm dd yyyy HH:MM:ss",
    shortDate:      "m/d/yy",
    mediumDate:     "mmm d, yyyy",
    longDate:       "mmmm d, yyyy",
    fullDate:       "dddd, mmmm d, yyyy",
    shortTime:      "h:MM TT",
    mediumTime:     "h:MM:ss TT",
    longTime:       "h:MM:ss TT Z",
    isoDate:        "yyyy-mm-dd",
    isoTime:        "HH:MM:ss",
    isoDateTime:    "yyyy-mm-dd'T'HH:MM:ss",
    isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"
};

// Nimble Internationalization strings
dateFormat.i18n = {
    dayNames: [
        "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
        "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
    ],
    monthNames: [
        "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
        "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
    ]
};

// For convenience...
Date.prototype.format = function (mask, utc) {
    return dateFormat(this, mask, utc);
};


 function getClientIp(req) {
    var ipAddress;
    // The request may be forwarded from local web server.
    var forwardedIpsStr = req.header('x-forwarded-for');
    if (forwardedIpsStr) {
        // 'x-forwarded-for' header may return multiple IP addresses in
        // the format: "client IP, proxy 1 IP, proxy 2 IP" so take the
        // the first one
        var forwardedIps = forwardedIpsStr.split(',');
        ipAddress = forwardedIps[0];
    }
    if (!ipAddress) {
        // If request was not forwarded
        ipAddress = req.connection.remoteAddress;
    }
    return ipAddress;
}

function mysha1( data ) {
    var generator = crypto.createHash('sha1');
    generator.update( data );
    return generator.digest('hex')
}

//temp hdnts

exports.akamai_token_v2_generator_hdnts = async function(req,res) {
    try {
        const tokenConfig = await getConfig('akamai', req.authParams.companyId);
        const config = {
            algorithm : 'SHA256',
            acl : '*',
            window : tokenConfig.window,
            key : tokenConfig.key,
            //ip: getClientIp(req),
            ip: req.ip.replace('::ffff:', ''),
            startTime:0,
            url:'',
            session:'',
            data:req.auth_obj.username,
            salt: tokenConfig.salt,
            delimeter:'~',
            escape_early:false,
            name:'hdnts'
        };

        let token = "?" + new akamai_token_generator.default(config).generateToken();
        let theresponse = new responses.OK();
        theresponse.extra_data = token;
        res.send(theresponse);
    }
    catch(err) {
        responses.send_res(req, res, [], 999, -1, 'DATABASE_ERROR_DESCRIPTION', 'DATABASE_ERROR_DATA', 'no-store');
    }
};



exports.akamai_token_v2_generator = async function(req,res) {
    try {
        const tokenConfig = await getConfig('akamai', req.authParams.companyId);
        const config = {
            algorithm : 'SHA256',
            acl : '*',
            window : tokenConfig.window,
            key : tokenConfig.key,
            //ip: getClientIp(req),
            ip: req.ip.replace('::ffff:', ''),
            startTime:0,
            url:'',
            session:'',
            data:req.auth_obj.username,
            salt: tokenConfig.salt,
            delimeter:'~',
            escape_early:false,
            name:'__token__'
        };

        let token = "?" + new akamai_token_generator.default(config).generateToken();
        let theresponse = new responses.OK();
            theresponse.extra_data = token;
        res.send(theresponse);
    }
    catch(err) {
        responses.send_res(req, res, [], 999, -1, 'DATABASE_ERROR_DESCRIPTION', 'DATABASE_ERROR_DATA', 'no-store');
    }
};

//returns the token as aditional query paramter
exports.akamai_token_v2_generator_extraquery = async function(req,res) {
    try {
        const tokenConfig = await getConfig('akamai', req.authParams.companyId);
        const config = {
            algorithm : 'SHA256',
            acl : '*',
            window : tokenConfig.window,
            key : tokenConfig.key,
            //ip: getClientIp(req),
            ip: req.ip.replace('::ffff:', ''),
            startTime:0,
            url:'',
            session:'',
            data:req.auth_obj.username,
            salt: tokenConfig.salt,
            delimeter:'~',
            escape_early:false,
            name:'__token__'
        };

        let token = "&" + new akamai_token_generator.default(config).generateToken();
        let theresponse = new responses.OK();
        theresponse.extra_data = token;
        res.send(theresponse);
    }
    catch(err) {
        responses.send_res(req, res, [], 999, -1, 'DATABASE_ERROR_DESCRIPTION', 'DATABASE_ERROR_DATA', 'no-store');
    }
};


exports.catchup_akamai_token_v2_generator = async function(req,res) {
    try {
        const tokenConfig = await getConfig('akamai', req.authParams.companyId);
        const config = {
            algorithm : 'SHA256',
            acl : '*',
            window : tokenConfig.window,
            key : tokenConfig.key,
            //ip: getClientIp(req),
            ip: req.ip.replace('::ffff:', ''),
            startTime:0,
            url:'',
            session:'',
            data:req.auth_obj.username,
            salt: tokenConfig.salt,
            delimeter:'~',
            escape_early:false,
            name:'token'
        };

        let token = "?" + new akamai_token_generator.default(config).generateToken();
        let theresponse = new responses.OK();
        theresponse.extra_data = token;
        res.send(theresponse);
    }
    catch(err) {
        responses.send_res(req, res, [], 999, -1, 'DATABASE_ERROR_DESCRIPTION', 'DATABASE_ERROR_DATA', 'no-store');
    }
};

exports.flussonic_token_generator =  async function(req, res) {
    try {
        const tokenConfig = await getConfig('flussonic', req.authParams.companyId);

        let token_key = tokenConfig.key; //server side only
        let password = req.query.password || tokenConfig.password; //Can be sent as query parameter
        let salt = req.query.salt || tokenConfig.salt; //Can be sent as query parameter

        let stream_name = req.params[0];
        let ip = req.query.ip || req.ip.replace('::ffff:', '');
        let starttime = req.query.starttime || Date.now()/1000|0;
        let endtime = req.query.endtime || (Date.now()/1000|0) + tokenConfig.window;

        let tohash = stream_name + ip + starttime + endtime + token_key + salt;

        let token = "?token="+mysha1(tohash)+ "-" + salt + "-" + endtime + "-" + starttime;

        let theresponse = new responses.OK();
            theresponse.extra_data = token;

        res.send(theresponse);
    }
    catch(err) {
        responses.send_res(req, res, [], 999, -1, 'DATABASE_ERROR_DESCRIPTION', 'DATABASE_ERROR_DATA', 'no-store');
    }
};

exports.flussonic_token__remote =  function(req, res) {
    var stream_name = req.params.stream_name;
    var token_url = req.query.tokenurl;
    var password = 'password';
    var salt = 'somesalt';
    var ip = req.ip.replace('::ffff:', '');
    var starttime = Date.now();
    var endtime = Date.now() + 1000;

    var queryparams = 'sing?' + 'password=' + password+'&name=' + stream_name + '&salt=' + salt + '&ip=' + ip + '&startime=' + starttime + '&endtime=' + endtime;

    request(token_url+queryparams, function (error, response, body) {
        var thisresponse = new responses.OK();

        if(response.statusCode === 200) {
            thisresponse.extra_data = body;
            res.send(thisresponse);
        }
        else {
            res.send(thisresponse);
        }
    });
};

/**
 * @api {get} /apiv2/token/nimble/:content name Request token Nimble Streamer
 * @apiName GetNimbleToken
 * @apiGroup TOKEN Generators
 *
 * @apiHeader {String} auth End User auth token.
 *
 */

exports.nimble_token_generator =  async function(req, res) {
    try {
        const tokenConfig = await getConfig('nimble_token', req.authParams.companyId);;
        const today = (new Date()).format("UTC:m/d/yyyy h:MM:ss TT");
        const ip = req.ip.replace('::ffff:', ''); //req.connection.remoteAddress;
        const str2hash = ip + tokenConfig.key + today + tokenConfig.window;

        let md5sum = crypto.createHash('md5');
        md5sum.update(str2hash, 'ascii');
        const base64hash = md5sum.digest('base64');

        const urlsignature = "server_time=" + today  + "&hash_value=" + base64hash + "&validminutes=" + tokenConfig.window;

        const base64urlsignature = new Buffer(urlsignature).toString('base64'); //todo: new buffer eshte depricated dhe duhet zevendesuar.

        const signedurlwithvalidinterval = "?wmsAuthSign=" + base64urlsignature;

        let thisresponse = new responses.OK();
            thisresponse.extra_data = signedurlwithvalidinterval;

        res.send(thisresponse);
    }
    catch(err) {
        responses.send_res(req, res, [], 999, -1, 'DATABASE_ERROR_DESCRIPTION', 'DATABASE_ERROR_DATA', 'no-store');
    }
};

exports.nimble_token_generatorThatReturns =  async function(req) {
    try {
        const tokenConfig = await getConfig('nimble_token', req.authParams.companyId);
        const today = (new Date()).format("UTC:m/d/yyyy h:MM:ss TT");
        const ip = getClientIp(req);
        const str2hash = ip + tokenConfig.key + today + tokenConfig.window;

        let md5sum = crypto.createHash('md5');
        md5sum.update(str2hash, 'ascii');
        const base64hash = md5sum.digest('base64');

        const urlsignature = "server_time=" + today  + "&hash_value=" + base64hash + "&validminutes=" + tokenConfig.window;

        const base64urlsignature = new Buffer(urlsignature).toString('base64');

        return "?wmsAuthSign=" + base64urlsignature
    }
    catch(err) {
        return null
    }
};

exports.handleGenerateTokenJson = async function(req, res) {
    try {
        const config = await getConfig('verizon', req.authParams.companyId);
        const token = generateECToken(req, config);
        const resp = {"status_code":200,"error_code":-1,"error_description":"","extra_data":"?" + token,"response_object":[]};
        res.send(resp);
    }
    catch(err) {
        responses.send_res(req, res, [], 999, -1, 'DATABASE_ERROR_DESCRIPTION', 'DATABASE_ERROR_DATA', 'no-store');
    }
}

exports.handleGenerateTokenJsonVerizonV4 = async function(req, res) {
    try {
        const config = await getConfig('verizon', req.auth.data.company_id);

        const token = generateECTokenV4(req, config);

        res.status(200).json({ token: `?${token}` });
    }
    catch(err) {
        responseHandler.sendError(req, res, 500, 999);
    }
    
}

function generateECTokenV4(req, config) {
    let expireAt = Date.now() + config.window * 1000;
    let ip = req.header('x-forwarded-for') || req.connection.remoteAddress;
    ip = ip.replace('::ffff:', '');

    let paramString = 'ec_expire=' + expireAt +
      '&ec_proto_allow=' + config.proto_allowed +
      '&ec_clientip=' + ip;

    return ectoken.encrypt(config.key, paramString);
}

function generateECToken(req, config) {
    let expireAt = Date.now() + config.window * 1000;
    let ip = req.header('x-forwarded-for') || req.connection.remoteAddress;
    ip = ip.replace('::ffff:', '');

    let paramString = 'ec_expire=' + expireAt + 
                        '&ec_proto_allow=' + config.proto_allowed +
                        '&ec_clientip=' + ip;

    let token = ectoken.encrypt(config.key, paramString);
    return token;
}

exports.nimble_drm_key = async function(req, res) {
    try {
        const tokenConfig = await getConfig('nimble_drm', req.authParams.companyId);
        const content = Buffer.from(tokenConfig.key, 'hex');
        res.setHeader('Content-Type', 'binary/octet-stream');
        res.end(content);
    }
    catch(err) {
        responses.send_res(req, res, [], 999, -1, 'DATABASE_ERROR_DESCRIPTION', 'DATABASE_ERROR_DATA', 'no-store');
    }
};

// Generate wowza HASH Token.
exports.wowza_token_generator = async function (req, res) {
    try {
        const tokenConfig = await getConfig('wowza', req.authParams.companyId);

        // The shared secret as generated in our Wowza application (Playback security tab) - use your own
        const sharedSecret = '6e94c7a77220aea1';
        //content path coming as query parameter, path=
        const contentPath = req.query.path;

        // The Hash Query Parameter Prefix as set in Wowza application
        const customSecureTokenPrefix = 'wowzatoken';
        // A custom paramater to add pepper and salt to security - use your own
        //var customParam = 'pepperandsalt';
        // date time start
        let startTime = Date.now()/ 1000 | 0 ; //'1440081611';
        startTime = startTime - 300;
        // duration in secconds, valid for 3 hrs
        let endTime = startTime + tokenConfig.window; //'1511704011';
        let toHash;
        toHash = contentPath + '?' +
            tokenConfig.shared_secret + '&' +
            //customSecureTokenPrefix + 'CustomParameter=' + customParam + '&' +
            customSecureTokenPrefix + 'endtime=' + endTime + '&' +
            customSecureTokenPrefix + 'starttime=' + startTime;

        let hash = crypto.createHash('sha256').
        update(toHash, "utf8").
        digest('base64');

        hash = hash.replace(/\//g, '_');
        hash = hash.replace(/\+/g, '-');

        let token = "?wowzatokenstarttime=" + startTime + "&wowzatokenendtime=" + endTime + "&wowzatokenhash=" + hash;

        let thisresponse = new responses.OK();
        thisresponse.extra_data = token;

        res.send(thisresponse);
    }
    catch(err) {
        responses.send_res(req, res, [], 999, -1, 'DATABASE_ERROR_DESCRIPTION', 'DATABASE_ERROR_DATA', 'no-store');
    }
}

/**
 * @api {GET} /apiv2/token/akamai/edgeauth Generate akamai token with edgeauth module
 * @apiName AkamaiEdgeAuth
 * @apiGroup StreamTokens
 * @apiParam (Query param) {String} stream_url Stream url
 * @apiParam (Query param) {String} tokenName Name of the token
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *           "status_code": 200,
 *           "error_code": 1,
 *           "timestamp": 1,
 *           "error_description": "OK",
 *           "extra_data": "tream url with token",
 *           "response_object": []
 *     }
 */
exports.handleGenerateAkamaiEdgeAuthToken = async function(req, res) {
    let streamUrl = req.query.stream_url;
    if (!streamUrl) {
        responses.send_res(req, res, [], 706, -1, 'BAD_REQUEST_DESCRIPTION', 'BAD_REQUEST_DATA', 'no-store');
        return;
    }
    let tokenName = req.query.tokenName;

    try {
        let tokenConfig = await getConfig('akamai', req.authParams.companyId);
        let pathName = url.parse(streamUrl).pathname;

        let ip = req.header('x-forwarded-for') || req.connection.remoteAddress;
        let token = generateAkamaiEdgeAuthToken(tokenConfig, tokenName, ip, pathName);
        let urlWithToken = streamUrl + '?' + token;
        let resp = new responses.OK();
        resp.extra_data = urlWithToken;

        res.send(resp);
    } 
    catch(e) {
        responses.send_res(req, res, [], 706, -1, 'BAD_REQUEST_DESCRIPTION', 'BAD_REQUEST_DATA', 'no-store');
    }
}

function generateAkamaiEdgeAuthToken(config, tokenName, ip, pathName) {
    if (!tokenName) {
        tokenName = '__token__';
    }

    let eaConfig = {
        key: config.key,
        windowSeconds: config.window,
        tokenName: tokenName,
        ip: ip,
        escapeEarly: false
    }

    let ea = new EdgeAuth(eaConfig);
    let token = ea.generateURLToken(pathName);
    token = tokenName  + '=' + token;
    return token;
}

exports.akamai_token_segment_media = function (req, res) {
    var config = {
        //algorithm : 'SHA256',
        acl: '*',
        window: req.app.locals.streamtokens.AKAMAISEGMENTMEDIA.WINDOW,
        key: 'a26c3de57cc0ba8a30d862bf770ab4dd', //req.app.locals.streamtokens.AKAMAI.TOKEN_KEY,
        //ip: getClientIp(req),
        ip: req.ip.replace('::ffff:', ''),
        startTime: 0,
        url: '',
        session: '',
        data: req.auth_obj.username,
        salt: req.app.locals.streamtokens.AKAMAISEGMENTMEDIA.SALT,
        delimeter: '~',
        escape_early: false,
        name: 'hdnts'
    };

    var token = "?" + new akamai_token_generator.default(config).generateToken();
    var theresponse = new responses.OK();
    theresponse.extra_data = token;
    res.send(theresponse);
};

exports.akamaiTokenSegmentMedia = async function (req, res) {
    try {
        const tokenConfig = await getConfig('akamai_segment_media', req.auth.data.company_id); 

        const config = {
            //algorithm : 'SHA256',
            acl: '*',
            window: tokenConfig.window,
            key: tokenConfig.key,
            ip:  getClientIp(req),
            startTime: 0,
            url: '',
            session: '',
            data: req.auth.data.username,
            salt: tokenConfig.salt,
            delimeter: '~',
            escape_early: false,
            name: 'hdnts'
        };

        let token = "?" + new akamai_token_generator.default(config).generateToken();
        responseHandler.sendData(req, res, { token })
    }
    catch(err) {
        responseHandler.sendError(req, res, 500, 999);
    }
};

exports.generateAkamaiEdgeAuthToken = generateAkamaiEdgeAuthToken;

exports.akamaiTokenNimbleOrigin = async function (req, res) {
    try {
        const tokenConfig = await getConfig('akamai', req.auth.data.company_id);

        const config = {
            algorithm: 'SHA256',
            acl: '*',
            window: tokenConfig.window,
            key: tokenConfig.key,
            ip: getClientIp(req),
            startTime: 0,
            url: '',
            session: '',
            data: req.auth.data.username,
            salt: req.app.locals.streamtokens.AKAMAI.SALT,
            delimeter: '~',
            escape_early: false,
            name: 'wmsAuthSign'
        };

        const token = "?" + new akamai_token_generator.default(config).generateToken();

        responseHandler.sendData(req, res, {token});
    }
    catch(err) {
        responseHandler.sendError(req, res, 500, 999);
    }
};